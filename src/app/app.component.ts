import {AfterViewInit, ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ApiService} from './services/api.service';
import {combineLatest, Observable, of, Subject} from 'rxjs';
import {Message} from './models/message';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {State} from './store/state';
import {select, Store} from '@ngrx/store';
import {addConversation} from './store/conversation.actions';
import {Conversation} from './models/conversation';
import {selectConversations, selectConversationState} from './store/conversation.selector';
import {addMessage} from './store/message.actions';
import {selectMessages} from './store/message.selector';
import {cloneDeep} from 'lodash';

interface Theme {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'ngpwa';

  isUserOnlineAndLoggedIn$: Observable<boolean>;
  isOnline$: Observable<boolean>;
  myNumber$: Observable<number[]>;
  messages$: Observable<Message[]>;

  searchValue: string;
  searchValue$ = new Subject<string>();

  themes: Theme[] = [
    {value: 'deepPurpleAmber', viewValue: 'Deep purple & amber'},
    {value: 'indigoPink', viewValue: 'Indigo & pink'},
    {value: 'pinkBlueGrey', viewValue: 'Pink & blue-grey'},
    {value: 'purpleGreen', viewValue: 'Purple & green'}
  ];

  selectedTheme = this.themes[0].value;

  conversations$: Observable<Conversation[]>;

  conversationIdStr: string;
  msg = 'message';

  constructor(
    private readonly apiService: ApiService,
    private readonly store: Store<State>
  ) {}

  ngOnInit(): void {
    this.isOnline$ = this.apiService.getOnlineStatus();
    this.isUserOnlineAndLoggedIn$ = this.apiService.isUserOnlineAndLoggedIn();
    this.myNumber$ = this.apiService.getNumber();

    this.messages$ = this.searchValue$.pipe(
      debounceTime(100),
      distinctUntilChanged(),
      switchMap(searchValue => this.apiService.searchMessages(searchValue))
    );

    this.conversations$ = combineLatest(this.store.pipe(select(selectConversations)), this.store.pipe(select(selectMessages)))
      .pipe(map((result: [Conversation[], Message[]]) => {
          const conversations = cloneDeep(result[0]);
          const messages = cloneDeep(result[1]);

          if (conversations) {
            conversations.forEach(conversation => {
              if (messages) {
                messages.forEach(message => {
                  if (conversation.id === message.conversationId) {
                    conversation.messages.push(message);
                  }
                });
              }
            });
          }
          return conversations;
        })
      );
  }

  ngAfterViewInit(): void {
    this.searchValue$.next(this.searchValue);
  }

  onSearch() {
    this.searchValue$.next(this.searchValue);
  }

  addConversation() {
    const conversation: Conversation = {id: 0, messages: []};
    this.store.dispatch(addConversation({conversation}));
  }

  addMessage(conversationIdStr: string, msg: string) {
    const conversationId = conversationIdStr ? parseInt(conversationIdStr, 10) : 0;
    const message: Message = {id: 0, msg, conversationId};
    this.store.dispatch(addMessage({message}));
  }
}
