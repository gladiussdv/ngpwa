import {createFeatureSelector, createSelector} from '@ngrx/store';
import {MessageState} from './message.state';

export const selectMessageState = createFeatureSelector<MessageState>('message');

export const selectMessages = createSelector(selectMessageState, messageState => messageState.entities);
