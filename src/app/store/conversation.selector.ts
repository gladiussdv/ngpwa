import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ConversationState} from './conversation.state';

export const selectConversationState = createFeatureSelector<ConversationState>('conversation');

export const selectConversations = createSelector(selectConversationState, conversationState => conversationState.entities);
