import {addConversation} from './conversation.actions';
// @ts-ignore
import {createReducer, on} from '@ngrx/store';
import {MessageState} from './message.state';
import {addMessage} from './message.actions';

const initialMessageState: MessageState = {
  entities: []
};

const messageReducer = createReducer(initialMessageState,
  on(addMessage, (state, action) => {
    const entities = [...state.entities];
    const id = entities.length;
    entities.push({...action.message, id});
    return {
      entities
    };
  })
);
export function messageReducerFunc(state, action) {
  return messageReducer(state, action);
}
