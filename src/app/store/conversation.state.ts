import {Conversation} from '../models/conversation';

export interface ConversationState {
  entities: Conversation[];
}
