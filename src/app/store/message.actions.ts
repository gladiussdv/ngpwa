import {createAction, props} from '@ngrx/store';
import {Message} from '../models/message';

export const addMessage = createAction('[Message] add', props<{ message: Message }>());
