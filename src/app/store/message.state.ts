import {Message} from '../models/message';

export interface MessageState {
  entities: Message[];
}
