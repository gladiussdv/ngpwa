import {addConversation} from './conversation.actions';
// @ts-ignore
import {createReducer, on} from '@ngrx/store';
import {ConversationState} from './conversation.state';

const initialConversationState: ConversationState = {
  entities: []
};

const conversationReducer = createReducer(initialConversationState,
  on(addConversation, (state, action) => {
    const entities = [...state.entities];
    const id = entities.length;
    entities.push({...action.conversation, id});
    return {
      entities
    };
  })
);
export function conversationReducerFunc(state, action) {
  return conversationReducer(state, action);
}
