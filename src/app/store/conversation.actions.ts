import {createAction, props} from '@ngrx/store';
import {Conversation} from '../models/conversation';

export const addConversation = createAction('[Conversation] add', props<{ conversation: Conversation }>());
