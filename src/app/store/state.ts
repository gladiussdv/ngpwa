import {ConversationState} from './conversation.state';
import {MessageState} from './message.state';

export interface State {
  conversation: ConversationState;
  message: MessageState;
}
