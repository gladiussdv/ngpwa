import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import {conversationReducerFunc} from './conversation.reducer';
import {State} from './state';
import {messageReducerFunc} from './message.reducer';



export const reducers: ActionReducerMap<State> = {
  conversation: conversationReducerFunc,
  message: messageReducerFunc
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
