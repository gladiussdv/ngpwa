export interface Message {
  id: number;
  msg: string;
  conversationId: number;
}
