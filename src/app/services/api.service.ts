import { Injectable } from '@angular/core';
import {combineLatest, fromEvent, interval, merge, Observable, of} from 'rxjs';
import {bufferTime, map, mapTo} from 'rxjs/operators';
import {Message} from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  isUserLoggedIn$: Observable<boolean>;

  myNumber$: Observable<number>;

  constructor() {
    this.isUserLoggedIn$ = of(true);
    this.myNumber$ = interval(500).pipe(map(() => Math.random()));
  }

  getOnlineStatus(): Observable<boolean> {
    return merge(
      of(navigator.onLine),
      fromEvent(window, 'online').pipe(mapTo(true)),
      fromEvent(window, 'offline').pipe(mapTo(false)),
    ).pipe(map(() => navigator.onLine));
  }

  isUserOnlineAndLoggedIn(): Observable<boolean> {
    const onlineStatus$ = this.getOnlineStatus();
    return combineLatest(this.isUserLoggedIn$, onlineStatus$).pipe(map((result: [boolean, boolean]) => {
      const isUserLoggedIn = result[1];
      const isOnline = result[0];
      return isUserLoggedIn && isOnline;
    }));
  }

  getNumber(): Observable<number[]> {
    return this.myNumber$.pipe(bufferTime(2000));
  }

  searchMessages(keyword: string): Observable<Message[]> {
    const messages: Message[] = [
      {id: 1, msg: 'first', conversationId: 0},
      {id: 2, msg: 'second', conversationId: 0},
      {id: 3, msg: 'third', conversationId: 0}
    ];
    return of(messages).pipe(
      map(messageList => messageList.filter(msg => {
        return msg.msg.includes(keyword || '');
      }))
    );
  }
}
